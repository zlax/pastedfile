<?php

    // pastedfile v.0.0.5-prealpha

    $mySQL[host]="localhost";
    $mySQL[user]="sqluser";
    $mySQL[password]="sqlpassword";
    $mySQL[dbname]="sqldbname";
    $mySQL[table]="sqltable";

    $SettingExpireDays="365";
    $SettingMaxFileSizeBytes="1048576";
    $DefaultMIMEtype="text/plain";

    $Titles[Head]="pastedfile";
    $Titles[404]="404";
    $Titles[ErrorDataTrans]="File data not transfered";
    $Titles[ErrorDataErrNo]="Error was occured during sending file (error number: ";
    $Titles[SuccFileSize]="File upload is succeful, size is: ";
    $Titles[SuccFileBytes]=" bytes";
    $Titles[ReqPasteID]="Pastedfile ID is required";
    $Titles[ReqPassword]="Password is required";
    $Titles[WrongPassword]="Wrong password";
    $Titles[WrongSymbols]="Please use only latin letters, numbers, \"_\",\"-\" and \".\"";
    $Titles[WrongSymbolsMIME]="MIME type can use only latin letters, numbers, \"-\", \"+\", \"/\" and \".\"";
    $Titles[ReqCaptcha]="Captcha is required";
    $Titles[WrongCaptcha]="Wrong captcha";
    $Titles[WrongPasteID]="This pastedfile ID already exists, please enter new unique Pastedfile ID";
    $Titles[addDone]="Well done. Your pastedfile ID was added, you can use it.";
    $Titles[ErrorAdd]="Can't add new pastedfile ID: ";
    $Titles[registerNew]="Register new pastedfile ID:";
    $Titles[PasteID]="Pastedfile ID";
    $Titles[Password]="Password";
    $Titles[Check]="check";
    $Titles[CheckSymbols]="You can use latin letters, numbers, \"_\",\"-\" and \".\". Text case sensitive.";
    $Titles[UploadFile]="Upload file";
    $Titles[NotNecessary]="not necessary";
    $Titles[MaximumSize]="Maximum allowed file size is: ";
    $Titles[Bytes]=" bytes";
    $Titles[Captcha]="Captcha";
    $Titles[Plus]="plus";
    $Titles[Expire]="Expire";
    $Titles[MIMEtype]="MIME type";
    $Titles[CanBeChanged]="it can be changed on edit page";
    $Titles[Submit]="Submit";
    $Titles[ExitButton]="Exit";
    $Titles[Data]="Data";
    $Titles[NewPassword]="New password";
    $Titles[Modified]="Modified";
    $Titles[PasswordUpdated]="Password was updated";
    $Titles[IndexRegister]="You can register place for new file there: ";
    $Titles[IndexGet]="You can get your file there: ";
    $Titles[IndexEdit]="You can edit your file settings there: ";
    $Titles[IndexPut]="You can upload your file via curl:";
    $Titles[SampleCurlPart1]="curl -F filedata=@/your/storage/your.file ";
    $Titles[SampleCurlPart2]="put/pastedfileID?password=yourpassword";
    $Titles[MaximumFIleSize]="Maximum file size: ";
    $Titles[DefaultMIME]="Default MIME type: ";
    $Titles[StoragePeriod]="File storage time: ";
    $Titles[Days]="days";

function MainURL()
    {
        if(isset($_SERVER['HTTPS'])) $pastedfileURL="https://";
            else $pastedfileURL="http://";
        $pastedfileURL=$pastedfileURL.$_SERVER['HTTP_HOST'].str_replace("index.php","",$_SERVER['PHP_SELF']);
        return $pastedfileURL;
    }

function ParsedURI()
    {
        $URIarray=array();
        if (!$_SERVER['QUERY_STRING']) $URIarray[]="index";
            else
            {
                $URIparts=preg_split("([/]|[&])", trim($_SERVER['QUERY_STRING'],' /&'));
                $URIparams=explode('&', trim($_SERVER['QUERY_STRING'], ' &'));
                foreach ($URIparts as $part) $URIarray[]=array_shift($URIparts);
                foreach ($URIparams as $param) $URIarray[param][]=array_shift($URIparams);
            }
        return $URIarray;
    }

function HTMLheader()
    {
        global $Titles;
        echo "<!DOCTYPE html><html><head><title>",$Titles[Head],"</title>\n";
        echo "<meta charset=\"utf-8\">\n";
        echo "<link rel=\"stylesheet\" href=\"",MainURL(),"anthrstlsht.css\">\n";
        echo "</head><body>\n";
        echo "<div class=\"container\">\n";
    }

function HTMLfooter()
    {
        echo "</div>\n";
        echo "</body></html>";
    }

function HTML404()
    {
        global $Titles;
        HTMLheader();
        echo "  <h2>",$Titles[404],"</h2>\n";
        HTMLfooter();
    }

if (ParsedURI()[0]=="index")
    {
        HTMLheader();
        echo "  <h4>",$Titles[Head],"</h4><br>\n";
        echo "  $Titles[IndexRegister]<a href=\"",MainURL(),"register\">",MainURL(),"register</a><br>\n";
        echo "  $Titles[IndexGet]<a href=\"",MainURL(),"get/pastedfileID\">",MainURL(),"get/pastedfileID</a><br>\n";
        echo "  $Titles[IndexEdit]<a href=\"",MainURL(),"edit/pastedfileID\">",MainURL(),"edit/pastedfileID</a><br>\n";
        echo "  $Titles[IndexPut]<br>\n";
        echo "  <pre><code>",$Titles[SampleCurlPart1],MainURL(),$Titles[SampleCurlPart2],"</code></pre>\n";
        echo "  ",$Titles[MaximumFIleSize],$SettingMaxFileSizeBytes," ",$Titles[Bytes],"<br>\n";
        echo "  ",$Titles[DefaultMIME],$DefaultMIMEtype,"<br>\n";
        echo "  ",$Titles[StoragePeriod],$SettingExpireDays," ",$Titles[Days],"<br>\n";
        HTMLfooter();
    }

else if (ParsedURI()[0]=="get")
    {
        if (!ParsedURI()[1]) HTML404();
            else
                {
                    $URIid=ParsedURI()[1];
                    $dbconnection=mysqli_connect($mySQL[host],$mySQL[user],$mySQL[password],$mySQL[dbname]);
                    mysqli_query($dbconnection,"SET CHARACTER SET 'utf8'");
                    $paste=mysqli_fetch_object(mysqli_query($dbconnection,"SELECT * FROM `$mySQL[table]` WHERE BINARY `id`='$URIid';"));
                    if ($paste->data)
                        {
                            $viewheader="Content-type: ".$paste->mime;
                            header($viewheader);
                            echo $paste->data;
                        }
                        else HTML404();
                }
    }

else if (ParsedURI()[0]=="put")
    {
        $URIid=ParsedURI()[1];
        $dbconnection=mysqli_connect($mySQL[host],$mySQL[user],$mySQL[password],$mySQL[dbname]);
        mysqli_query($dbconnection,"SET CHARACTER SET 'utf8'");
        $paste=mysqli_fetch_object(mysqli_query($dbconnection,"SELECT * FROM `".$mySQL[table]."` WHERE `id` = '$URIid';"));
        $URIparam=explode('=',ParsedURI()[param][1]);
        if ($URIid===$paste->id)
            if (($URIparam[0]=="password")&&($URIparam[1]===$paste->password))
                {
                    header("Content-type: text/plain");
                    if ($_FILES['filedata']['size']<1) echo $Titles[ErrorDataTrans],"\n";
                    if ($_FILES['filedata']['size']>$SettingMaxFileSizeBytes) echo $Titles[ErrorDataTrans],". ",$Titles[MaximumSize],$SettingMaxFileSizeBytes,$Titles[Bytes],"\n";
                    if ($_FILES['filedata']['error']>0) echo $Titles[ErrorDataErrNo],$_FILES['filedata']['error'],")\n";
                        else
                            {
                                if ($SettingMaxFileSizeBytes>$_FILES['filedata']['size']&&$_FILES['filedata']['size']>0)
                                    {
                                        echo $Titles[SuccFileSize],$_FILES['filedata']['size'],$Titles[SuccFileBytes],"\n";
                                        $filedata=addslashes(fread(fopen($_FILES['filedata']['tmp_name'],"rb"),filesize($_FILES['filedata']['tmp_name'])));
                                        mysqli_query($dbconnection,"UPDATE `$mySQL[table]` SET `data` = '$filedata' WHERE `$mySQL[table]`.`id` = '$URIid';");
                                        mysqli_query($dbconnection,"UPDATE `$mySQL[table]` SET `modified` = NOW( ) WHERE `$mySQL[table]`.`id` = '$URIid';");
                                    }
                            }
                }
                else HTML404();
            else HTML404();
    }

else if (ParsedURI()[0]=="register")
    {
        session_start();
        HTMLheader();
        if($_POST['submit'])
            {
                $_SESSION['formpasteid']=$_POST['pasteid'];
                $UncorrectPasteIDorPassword=0;
                if(!$_POST['pasteid']) echo "<i>",$Titles[ReqPasteID],"</i><br>\n<br>\n";
                if((!$_POST['password'])||(!$_POST['password2'])) echo "<i>",$Titles[ReqPassword],"</i><br>\n<br>\n";
                    else if($_POST['password']!=$_POST['password2']) echo "<i>",$Titles[WrongPassword],"</i><br>\n<br>\n";
                if(($_POST['pasteid'])&&(!ereg("^[a-zA-Z0-9_\.-]+$",$_POST['pasteid']))) $UncorrectPasteIDorPassword=1;
                if(($_POST['password'])&&(!ereg("^[a-zA-Z0-9_\.-]+$",$_POST['password']))) $UncorrectPasteIDorPassword=1;
                if(!$_POST['captcha']) echo "<i>",$Titles[ReqCaptcha],"</i><br>\n<br>\n";
                if (($_POST['captcha'])&&!($_POST["captcha"]==$_SESSION["captcha"])) echo "<i>",$Titles[WrongCaptcha],"</i><br>\n<br>\n";
                if ($UncorrectPasteIDorPassword==1) echo "<i>",$Titles[WrongSymbols],"</i><br>\n<br>\n";
                    else
                        {
                            if ((($_POST['captcha'])&&($_POST["captcha"]==$_SESSION["captcha"]))&&(($_POST['pasteid'])&&($_POST['password']))&&($_POST['password']===$_POST['password2']))
                                {
                                    $dbconnection=mysqli_connect($mySQL[host],$mySQL[user],$mySQL[password],$mySQL[dbname]);
                                    mysqli_query($dbconnection,"SET CHARACTER SET 'utf8'");
                                    mysqli_query($dbconnection,"INSERT INTO `".$mySQL[table]."` (`id`,`password`,`data`,`modified`,`expire`,`mime`) VALUES ('".$_POST['pasteid']."','".$_POST['password']."',NULL,CURRENT_TIMESTAMP ,'".date("Y-m-d h:i:s", $_SESSION['expiretimestamp'])."','".$DefaultMIMEtype."');");
                                    if (mysqli_errno($dbconnection)==1062) echo "<i>",$Titles[WrongPasteID],"</i><br>\n<br>\n";
                                        else if (mysqli_errno($dbconnection)==0)
                                            {
                                                $CorrectPasteID="true";
                                                echo $Titles[addDone],"<br>\n";
                                                $_SESSION['formpasteid']="";
                                                if ($SettingMaxFileSizeBytes>$_FILES['filedata']['size']&&$_FILES['filedata']['size']>0)
                                                    {
                                                        $filedata=addslashes(fread(fopen($_FILES['filedata']['tmp_name'],"rb"),filesize($_FILES['filedata']['tmp_name'])));
                                                        mysqli_query($dbconnection,"UPDATE `$mySQL[table]` SET `data` = '$filedata' WHERE `$mySQL[table]`.`id` = '".$_POST['pasteid']."';");
                                                    }
                                            }
                                        else echo "<i>",$Titles[ErrorAdd],mysqli_error($dbconnection),"</i><br>\n<br>\n";
                                }
                        }
            }
        if (!($_POST['submit'])||!($CorrectPasteID))
            {
                $captchanumber1=rand(1, 50);
                $captchanumber2=rand(1, 49);
                $_SESSION['captcha']=$captchanumber1+$captchanumber2;
                $_SESSION['expiretimestamp']=(time()+$SettingExpireDays*24*60*60);
                echo "  <h4>",$Titles[registerNew],"</h4>\n";
                echo "<form action=register method=post enctype=multipart/form-data>\n";
                echo "  ",$Titles[PasteID],": <input type=text name=\"pasteid\" value=\"",$_SESSION['formpasteid'],"\" maxlength=255><br>\n";
                echo "  ",$Titles[Password],": <input type=password name=\"password\" maxlength=255><br>\n";
                echo "  ",$Titles[Password],": <input type=password name=\"password2\" maxlength=255> (",$Titles[Check],")<br>\n";
                echo "  ",$Titles[CheckSymbols],"<br>\n";
                echo "  ",$Titles[UploadFile],": <input type=file name=\"filedata\"> (",$Titles[NotNecessary],")<br>\n";
                echo "  ",$Titles[MaximumSize],$SettingMaxFileSizeBytes,$Titles[Bytes],"<br>\n";
                echo "  ",$Titles[Expire],": ",date("Y m d", $_SESSION['expiretimestamp']),"<br>\n";
                echo "  ",$Titles[MIMEtype],": ",$DefaultMIMEtype," (",$Titles[CanBeChanged],")<br>\n";
                echo "  ",$Titles[Captcha],": $captchanumber1 ",$Titles[Plus]," $captchanumber2 = <input type=text name=\"captcha\" size=2 maxlength=2> ?<br>\n";
                echo "  <input type=submit name=\"submit\" value=\"",$Titles[Submit],"\">\n";
                echo "</form>\n";
            }
        HTMLfooter();
    }

else if (ParsedURI()[0]=="edit")
    {
        if (ParsedURI()[1])
            {
                $URIid=ParsedURI()[1];
                $dbconnection=mysqli_connect($mySQL[host],$mySQL[user],$mySQL[password],$mySQL[dbname]);
                mysqli_query($dbconnection,"SET CHARACTER SET 'utf8'");
                $paste=mysqli_fetch_object(mysqli_query($dbconnection,"SELECT * FROM `".$mySQL[table]."` WHERE `id` = '$URIid';"));
                if ($URIid===$paste->id)
                    {
                        session_start();
                        HTMLheader();
                        if($_POST['submit'])
                            {
                                if ($_POST['password']===$paste->password) $_SESSION['editstatus']=$paste->id;
                                    else echo "<i>",$Titles[WrongPassword],"</i><br>\n<br>\n";
                            }
                        if($_POST['exit']) $_SESSION['editstatus']="";
                        if($_POST['submit2'])
                            {
                                $editerror="";
                                if ($SettingMaxFileSizeBytes>$_FILES['filedata']['size']&&$_FILES['filedata']['size']>0)
                                    {
                                        $filedata=addslashes(fread(fopen($_FILES['filedata']['tmp_name'],"rb"),filesize($_FILES['filedata']['tmp_name'])));
                                        mysqli_query($dbconnection,"UPDATE `$mySQL[table]` SET `data` = '$filedata' WHERE `$mySQL[table]`.`id` = '".$paste->id."';");
                                        mysqli_query($dbconnection,"UPDATE `$mySQL[table]` SET `modified` = NOW( ) WHERE `$mySQL[table]`.`id` = '".$paste->id."';");
                                    }
                                if ($_POST['mime']!=$paste->mime)
                                    {
                                        if(!ereg("^[a-zA-Z0-9\./+-]+$",$_POST['mime'])) $editerror=$editerror."<i>".$Titles[WrongSymbolsMIME]."</i><br><br>\n";
                                            else mysqli_query($dbconnection,"UPDATE `$mySQL[table]` SET `mime` = '".$_POST['mime']."' WHERE `$mySQL[table]`.`id` = '".$paste->id."';");
                                    }
                                if($_POST['password']||$_POST['password2'])
                                    {
                                        if($_POST['password']===$_POST['password2'])
                                            {
                                                if(!ereg("^[a-zA-Z0-9_\.-]+$",$_POST['password'])) $editerror=$editerror."<i>".$Titles[WrongSymbols]."</i><br><br>\n";
                                                    else
                                                        {
                                                            mysqli_query($dbconnection,"UPDATE `$mySQL[table]` SET `password` = '".$_POST['password']."' WHERE `$mySQL[table]`.`id` = '".$paste->id."';");                                                
                                                            $editerror=$editerror."<i>".$Titles[PasswordUpdated]."</i><br><br>\n";
                                                        }
                                            }
                                            else $editerror=$editerror."<i>".$Titles[WrongPassword]."</i><br><br>\n";
                                    }
                                $paste=mysqli_fetch_object(mysqli_query($dbconnection,"SELECT * FROM `".$mySQL[table]."` WHERE `id` = '$URIid';"));
                            }
                        if($_SESSION['editstatus']==$URIid)
                            {
                                echo "<form action=",$URIid," method=post enctype=multipart/form-data>\n";
                                echo "  <h4>",$Titles[PasteID],": ",$paste->id,"</h4><br>\n";
                                echo "  ",$editerror;
                                echo "  ",$Titles[Data],": ";
                                    if (strlen($paste->data)>0) echo "<a href=\"",MainURL(),"get/",$paste->id,"\">",strlen($paste->data),$Titles[Bytes],"</a><br>\n";
                                        else echo "0",$Titles[Bytes],"<br>\n";
                                echo "  ",$Titles[Modified],": ",$paste->modified,"<br>\n";
                                echo "  ",$Titles[Expire],": ",$paste->expire,"<br>\n";
                                echo "  ",$Titles[UploadFile],": <input type=file name=\"filedata\"><br>\n";
                                echo "  ",$Titles[MIMEtype],": <input type=text name=\"mime\" value=\"",$paste->mime,"\" maxlength=255><br>\n";
                                echo "  ",$Titles[NewPassword],": <input type=password name=\"password\" maxlength=255><br>\n";
                                echo "  ",$Titles[NewPassword],": <input type=password name=\"password2\" maxlength=255> (",$Titles[Check],")<br>\n";
                                echo "  <input type=submit name=\"submit2\" value=\"",$Titles[Submit],"\"><br><br>\n";
                                echo "  <input type=submit name=\"exit\" value=\"",$Titles[ExitButton],"\">\n";
                                echo "</form>\n";
                            }
                            else
                            {
                                echo "<form action=",$URIid," method=post>\n";
                                echo "  ",$Titles[Password],": <input type=password name=\"password\" maxlength=255><br>\n";
                                echo "  <input type=submit name=\"submit\" value=\"",$Titles[Submit],"\">\n";
                                echo "</form>\n";
                            }
                        HTMLheader();
                    }
                    else HTML404();
            } else HTML404();
    }

else HTML404();

?>
